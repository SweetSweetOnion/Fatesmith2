using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[DefaultExecutionOrder(1000)]
public class Game : MonoBehaviour
{
	private static Game _instance;
	public static Game Instance { get { return _instance ? _instance : FindObjectOfType<Game>(); } }
	public BattleManager Battle { get; private set; }
	public ArmyManager ArmyManager { get; private set; }

	public ArmyComponent playerArmyPrefab;
	public ArmyComponent enemyArmyPrefab;

	public int turnBeforeFight = 3;
	public int currentTurn = 0;
	private int nextFight;

	//EVENTS
	public delegate void CharacterEnter(CharacterComponent c);
	public event CharacterEnter OnCharacterEnter;
	public delegate void GamePhase();
	public event GamePhase OnEndTurn;
	public event GamePhase OnBattleSolve;



	private void Awake()
	{
		_instance = this;
		Battle = GetComponentInChildren<BattleManager>();
		ArmyManager = GetComponentInChildren<ArmyManager>();
	}
	private void Start()
	{
		ArmyManager.InitArmy();
		nextFight = turnBeforeFight;
		OnCharacterEnter?.Invoke(ArmyManager.playerArmy.GetCurrentSoldier());
		OnEndTurn?.Invoke();
	}

	private void Update()
	{
		/*if(Input.GetKeyUp(KeyCode.U)){
			Upgrade();
		}*/
		if (Input.GetKeyUp(KeyCode.E))
		{
			EndTurn();
		}
	}

	public void Upgrade(){
		/*var current = ArmyManager.playerArmy.GetCurrentSoldier();
		if (!current || current.isDead ){
			current = ArmyManager.playerArmy.GetNextAliveSoldier();
		}
		if(current){
			current.Upgrade();
		}*/
	}

	public void EndTurn()
	{
		currentTurn++;
		nextFight--;
		if(nextFight <= 0){
			SolveFight();
			nextFight = turnBeforeFight;
		}else{
		}
		var c = ArmyManager.playerArmy.GetNextAliveSoldier();
		OnCharacterEnter?.Invoke(c);
		OnEndTurn?.Invoke();
	}

	public void SolveFight(){
		
		Battle.SolveFight();
		
		Debug.Log("END FIGHT");
		Debug.Log("Enemy remaining : " + ArmyManager.enemyArmy.CountAliveSoldiers() + "/" + ArmyManager.enemyArmy.CountSoldiers());
		Debug.Log("Soldier remaining : " + ArmyManager.playerArmy.CountAliveSoldiers() + "/" + ArmyManager.playerArmy.CountSoldiers());

		if (ArmyManager.enemyArmy.CountAliveSoldiers() <= ArmyManager.playerArmy.CountAliveSoldiers()/3)
		{
			ArmyManager.enemyArmy.Reinforce(currentTurn/2);
		}
		OnBattleSolve?.Invoke();
	}
	
}
