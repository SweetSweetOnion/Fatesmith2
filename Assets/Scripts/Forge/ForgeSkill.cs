﻿using System.Collections;
using UnityEngine;
using Sirenix.OdinInspector;

[CreateAssetMenu(fileName = "ForgeSkill", menuName = "Data/ForgeSkill")]
[InlineEditor]
public class ForgeSkill : ScriptableObject
{
	[BoxGroup("Effect")]
	public float qualityAdd;
	[BoxGroup("Effect")]
	public float temperatureAdd;
	[BoxGroup("Effect")]
	public int emberAdd;
	[BoxGroup("Effect")]
	public float cooldown;


	[BoxGroup("Visual")]
	public Sprite skillSprite;
	[BoxGroup("Visual")]
	public string description;

}
