﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
public class BladeForge : MonoBehaviour
{
	public BlacksmithController controller;
	public ForgeManager manager;
	public ForgeGauge gauge;
	public TextMeshProUGUI metalUi;
	public TextMeshProUGUI scoreUI;

	public int hammerCount;
	public float hammerStartTime;

	public float lastHammerTime;
	public UIBonus bonus;

	public Vector2Int minMaxHammerValue = new Vector2Int(5, 10);
	public Vector2Int minMaxRandomBonus = new Vector2Int(5, 10);
	public int maxQuality = 12;
	public float maxHammerTime = 1.5f;
	public int rotateBladeReduction = 3;
	public int hammerMetalCost = 1;


	public int currentMetal = 20;

	private int score;



	

	private void OnEnable()
	{
		controller.hammer.OnStart += Hammer_OnStart;
		controller.hammer.OnEnd += Hammer_OnEnd;
		controller.rotateBlade.OnEnd += RotateBlade_OnEnd;
		ForgeManager.OnCharacterSwitch += ForgeManager_OnCharacterSwitch;
		ForgeManager.OnCharacterleave += ForgeManager_OnCharacterleave;
	}



	private void OnDisable()
	{
		controller.hammer.OnStart -= Hammer_OnStart;
		controller.hammer.OnEnd -= Hammer_OnEnd;
		controller.rotateBlade.OnEnd -= RotateBlade_OnEnd;
		ForgeManager.OnCharacterleave -= ForgeManager_OnCharacterleave;

	}

	private void ForgeManager_OnCharacterleave()
	{
		if (manager.currentCharacter.weaponQuality >= 10)
		{
			currentMetal += 3;
		}else
		if (manager.currentCharacter.weaponQuality > maxQuality)
		{
			currentMetal += 6;
		}
		if (manager.currentCharacter.weaponQuality <= maxQuality)
		{
			score += manager.currentCharacter.weaponQuality;
		}
		scoreUI.text = score + "";
		metalUi.text = currentMetal + " metal";
	}

	private void ForgeManager_OnCharacterSwitch()
	{
		
		gauge.SetGauge(manager.currentCharacter.weaponQuality, maxQuality);
	}

	private void RotateBlade_OnEnd()
	{
		hammerCount = 0;
		manager.currentCharacter.weaponQuality -= rotateBladeReduction;
		UIBonus b = Instantiate(bonus, transform.position, Quaternion.identity, transform);
		b.color = Color.grey;
		b.text = manager.currentCharacter.weaponQuality + "";
		gauge.SetGauge(manager.currentCharacter.weaponQuality, maxQuality);

	}

	private void Hammer_OnStart()
	{
		hammerStartTime = Time.time;
	}

	private void Hammer_OnEnd()
	{
		if (currentMetal < hammerMetalCost) return;

		currentMetal -= hammerMetalCost;

		float hammerLoadDuration = Time.time - hammerStartTime;
		float normalizeDelay = Mathf.Clamp01(hammerLoadDuration / maxHammerTime);
		int hammerHit = Mathf.FloorToInt( Mathf.Lerp(minMaxHammerValue.x, minMaxHammerValue.y, normalizeDelay));
		hammerHit += Random.Range(minMaxRandomBonus.x, minMaxRandomBonus.y);

		lastHammerTime = Time.time;
		UIBonus b = Instantiate(bonus, transform.position, Quaternion.identity,transform);

		manager.currentCharacter.weaponQuality += hammerHit;
		gauge.SetGauge(manager.currentCharacter.weaponQuality, maxQuality);

		if (manager.currentCharacter.weaponQuality > maxQuality)
		{
			
			b.duration = 2;
			b.color = Color.red;
			
			b.text = "BROKEN!!!" + manager.currentCharacter.weaponQuality;
			manager.MakeCharacterLeave();
		}
		else if(manager.currentCharacter.weaponQuality == maxQuality)
		{
			b.duration = 2;
			b.color = Color.green;

			b.text = "PERFECT";
			manager.MakeCharacterLeave();
		
		}
		else
		{
			b.duration = 1;
			b.color = Color.yellow;
			b.text = ""+ manager.currentCharacter.weaponQuality;
		}


		metalUi.text = currentMetal+" metal";
	}


	
}
