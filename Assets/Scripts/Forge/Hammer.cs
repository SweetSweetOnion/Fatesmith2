﻿using System.Collections;
using UnityEngine;

public class Hammer : MonoBehaviour
{
	public ParticleSystem particle;
	public ForgeSkill hammerSkill;

	public Animator anim;


	private float currentCooldDown;
	private bool isBuffered = false;

	public delegate void UseHammer(ForgeSkill f);
	public event UseHammer OnUseHammer;


	// Use this for initialization
	void Start()
	{

	}

	// Update is called once per frame
	void Update()
	{
		if (Input.GetMouseButtonUp(0))
		{
			if (currentCooldDown <= 0)
			{
				ApplyHammer();
			}			
		}

		if (Input.GetMouseButton(0))
		{
			
			PrepareHammer();
			if (currentCooldDown > 0)
			{
				isBuffered = true;
			}
			
		}
		else
		{
			if (currentCooldDown <= 0 && isBuffered)
			{
				ApplyHammer();
			}
		}


		if (Input.GetMouseButtonDown(1))
		{
			if (anim)
			{
				anim.SetBool("Rotate", true);
			}
		}
		else
		{
			if (anim)
			{
				anim.SetBool("Rotate", false);
			}
		}



			currentCooldDown = Mathf.Max(currentCooldDown - Time.deltaTime,0);
		
	}

	
	private void PrepareHammer()
	{
		if (anim)
		{
			anim.SetBool("PrepareHammer", true);
		}
	}

	private void ApplyHammer()
	{
		isBuffered = false;
		currentCooldDown = hammerSkill.cooldown;

		if(particle)
		{
			particle.transform.position = Camera.main.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, 10));
			particle.Play();

			var m = particle.main;
			m.loop = false;
		}

		if (anim)
		{
			//anim.SetBool("Hammer",true);
			anim.SetBool("PrepareHammer", false);
		}

		OnUseHammer?.Invoke(hammerSkill);
		
	}
}
