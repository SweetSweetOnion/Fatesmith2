﻿using System.Collections;
using UnityEngine;

public class ForgeCharacter
{
	public string characterName;
	public int visitCounter = 0;
	public int weaponQuality = 0;
	public Sprite sprite;

}
