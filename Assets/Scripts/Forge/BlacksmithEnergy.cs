﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class BlacksmithEnergy : MonoBehaviour
{
	public float currentEnergy;
	public float maxEnergy;
	public float hammerCost = 1;

	public BlacksmithController controller;
	public Image gauge;

	private void OnEnable()
	{
		controller.hammer.OnEnd += Hammer_OnEnd;
		controller.rotateBlade.OnEnd += RotateBlade_OnEnd;
	}

	private void OnDisable()
	{
		controller.hammer.OnEnd -= Hammer_OnEnd;
		controller.rotateBlade.OnEnd -= RotateBlade_OnEnd;
	}

	private void RotateBlade_OnEnd()
	{
		
	}

	private void Hammer_OnEnd()
	{
		currentEnergy -= hammerCost;
		if(currentEnergy <= 0)
		{
			Debug.Log("game over");
		}
	}

	private void Update()
	{
		gauge.fillAmount = currentEnergy / maxEnergy;
	}
}
