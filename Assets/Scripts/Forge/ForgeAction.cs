﻿using System.Collections;
using UnityEngine;
using System;

[Serializable]
public class ForgeAction
{
	public delegate void StateEvent();
	public event StateEvent OnStart;
	public event StateEvent OnRunning;
	public event StateEvent OnEnd;
	public event StateEvent OnCanceled;

	public Vector2 minMaxStartDuration = new Vector2(-1,-1);
	public float runDuration = -1;
	public bool canBeBuffered = false;
	public float cooldown; 
	

	private bool isBuffered;
	private bool endStart = false;
	
	private float currentPhaseDuration;
	private float currentCooldown = 0;

	private bool _hasBeenCanceled = false;
	public bool HasBeenCanceled { get { return _hasBeenCanceled; } }

	public ActionPhase CurrentPhase { get { return _currentPhase; } }
	private ActionPhase _currentPhase = ActionPhase.None;
	public enum ActionPhase
	{
		None,
		Start,
		Running
	}

	public void Start()
	{
		if (_currentPhase != ActionPhase.None) return;
		if(currentCooldown <= 0)
		{
			isBuffered = false;
			_currentPhase = ActionPhase.Start;
			currentPhaseDuration = 0;
			OnStart?.Invoke();
			
		}
		else
		{
			if (canBeBuffered)
			{
				isBuffered = true;
			}
		}
		_hasBeenCanceled = false;
	}

	public void EndStart()
	{
		if (_currentPhase != ActionPhase.Start) return;
		endStart = true;
	}
	public void Cancel()
	{
		if (_currentPhase == ActionPhase.None) return;
		_currentPhase = ActionPhase.None;
		currentPhaseDuration = 0;
		currentCooldown = cooldown;
		_hasBeenCanceled = true;
		OnCanceled?.Invoke();

	}

	private void Run()
	{
		if (_currentPhase != ActionPhase.Start) return;

		endStart = false;
		_currentPhase = ActionPhase.Running;
		currentPhaseDuration = 0;
		OnRunning?.Invoke();	
	}

	private void End()
	{
		if (_currentPhase != ActionPhase.Running) return;
		_currentPhase = ActionPhase.None;
		currentPhaseDuration = 0;
		currentCooldown = cooldown;
		OnEnd?.Invoke();	
	}

	public void Update(float deltaTime)
	{
		if (_currentPhase == ActionPhase.None && isBuffered && currentCooldown <= 0)
		{
			Start();
			
		}

		switch (_currentPhase)
		{
			case ActionPhase.None:
				break;
			case ActionPhase.Start:
				
				if (endStart && minMaxStartDuration.x >= 0 && currentPhaseDuration >= minMaxStartDuration.x)
				{
					Run();
				}
				else
				{
					if (minMaxStartDuration.y >= 0 && currentPhaseDuration >= minMaxStartDuration.y)
					{
						Run();
					}
				}
				
				break;
			case ActionPhase.Running:
				if (runDuration >= 0 && currentPhaseDuration >= runDuration)
				{
					End();
				}
				break;
			default:
				break;
		}

		currentCooldown = Mathf.Max(currentCooldown - deltaTime, 0);
		currentPhaseDuration += deltaTime;
	}

}
