﻿using System.Collections;
using UnityEngine;

public class BlacksmithController : MonoBehaviour
{

	public ForgeAction hammer;
	public ForgeAction rotateBlade;
	public ForgeManager manager;
	


	void Start()
	{

	}

	void Update()
	{
		

		if (manager.isForging)
		{
			//hammer

			if (Input.GetMouseButtonDown(0))
			{
				hammer.Start();
			}

			

			if (Input.GetMouseButtonUp(0))
			{
				hammer.EndStart();
			}

			if (Input.GetMouseButton(0) && hammer.CurrentPhase == ForgeAction.ActionPhase.None && !hammer.HasBeenCanceled)
			{
				hammer.Start();
			}

			if (!Input.GetMouseButton(0) && hammer.CurrentPhase == ForgeAction.ActionPhase.Start)
			{
				hammer.EndStart();
			}

			if (Input.GetMouseButtonDown(1))
			{
				//rotate blade
				if (hammer.CurrentPhase == ForgeAction.ActionPhase.None)
				{
					rotateBlade.Start();
				}
				else
				{
					hammer.Cancel();
				}
			}
				
			
		}
		else
		{
			if(hammer.CurrentPhase != ForgeAction.ActionPhase.None){
				hammer.Cancel();
			}
		}

		hammer.Update(Time.deltaTime);
		rotateBlade.Update(Time.deltaTime);


	}
}
