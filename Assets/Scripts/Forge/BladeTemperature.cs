﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;


public class BladeTemperature : MonoBehaviour
{
	public Image gauje;
	public BlacksmithController controller;

	public float currentTemperature = 950f;
	public float temperatureAcc = 0;
	public float temperatureSpeed = 0;
	

	public Vector2 minMaxTemperature = new Vector2(500, 1400);
	public float maxTermperatureSpeedDecrease = -50;
	public float temperatureDecrease= -10;
	public float speedDrag = -0.1f;


	public float hammerSpeed;
	public float hammerTemp;


	private void OnEnable()
	{
		controller.hammer.OnEnd += Hammer_OnEnd;
	}

	private void OnDisable()
	{
		controller.hammer.OnEnd -= Hammer_OnEnd;

	}

	private void Hammer_OnEnd()
	{
		temperatureSpeed += hammerSpeed;
		currentTemperature += hammerTemp;
	}

	

	private void Update()
	{
		temperatureAcc += temperatureDecrease;
		temperatureAcc += speedDrag * Mathf.Sign(temperatureSpeed) * temperatureSpeed * temperatureSpeed;

		temperatureSpeed += temperatureAcc * Time.deltaTime;

		temperatureSpeed = Mathf.Max(temperatureSpeed, maxTermperatureSpeedDecrease);

		currentTemperature +=  temperatureSpeed * Time.deltaTime;

		currentTemperature = Mathf.Max(Mathf.Min(currentTemperature, minMaxTemperature.y), minMaxTemperature.x);

		temperatureAcc = 0;

		gauje.fillAmount = Mathf.InverseLerp(minMaxTemperature.x, minMaxTemperature.y, currentTemperature);
	}

}
