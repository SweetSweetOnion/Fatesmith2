﻿using System.Collections;
using UnityEngine;


public class BlacksmithAnim : MonoBehaviour
{

	public BlacksmithController controller;
	public Animator animator;
	public AudioSource audioSource;
	public AudioClip hammerClip;
	public AudioClip rotateClip;


	private void OnEnable()
	{
		controller.hammer.OnStart += Hammer_OnStart;
		controller.hammer.OnRunning += Hammer_OnRunning;
		controller.hammer.OnEnd += Hammer_OnEnd;
		controller.hammer.OnCanceled += Hammer_OnCanceled;
		controller.rotateBlade.OnStart += RotateBlade_OnStart;
		controller.rotateBlade.OnRunning += RotateBlade_OnRunning;
		controller.rotateBlade.OnEnd += RotateBlade_OnEnd;
	}

	

	private void OnDisable()
	{
		controller.hammer.OnStart -= Hammer_OnStart;
		controller.hammer.OnRunning -= Hammer_OnRunning;
		controller.hammer.OnEnd -= Hammer_OnEnd;
		controller.hammer.OnCanceled -= Hammer_OnCanceled;
		controller.rotateBlade.OnStart -= RotateBlade_OnStart;
		controller.rotateBlade.OnRunning -= RotateBlade_OnRunning;
		controller.rotateBlade.OnEnd -= RotateBlade_OnEnd;
	}

	private void Hammer_OnStart()
	{
		animator.SetBool("PrepareHammer", true);
		animator.SetBool("Hammering", false);
	}
	private void Hammer_OnRunning()
	{
		animator.SetBool("PrepareHammer", false);
		animator.SetBool("Hammering", true);
	}

	private void Hammer_OnEnd()
	{
		animator.SetBool("PrepareHammer", false);
		animator.SetBool("Hammering", false);
		audioSource.volume = 1;
		audioSource.PlayOneShot(hammerClip);
	}
	private void Hammer_OnCanceled()
	{
		animator.SetBool("PrepareHammer", false);
		animator.SetBool("Hammering", false);
	}



	private void RotateBlade_OnStart()
	{
		animator.SetBool("Rotate", true);
		audioSource.volume = 0.1f;
		audioSource.PlayOneShot(rotateClip);
		
	}
	private void RotateBlade_OnRunning()
	{
		
	}
	private void RotateBlade_OnEnd()
	{
		animator.SetBool("Rotate", false);
	}




}
