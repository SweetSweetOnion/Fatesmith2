﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class ForgeGauge : MonoBehaviour
{
	public Image gauge;
	public TextMeshProUGUI textMesh;


	public void SetGauge(float value, float maxValue)
	{
		gauge.fillAmount = value / maxValue;
		textMesh.text = value + "/" + maxValue;
		if(value > maxValue)
		{
			gauge.color = Color.red;

		}
		else
		{
			if (value == maxValue)
			{
				gauge.color = Color.yellow;
			}else if(value >= 10)
			{
				gauge.color = Color.green;
			}
			else
			{
				gauge.color = Color.grey;
			}

		}
	}
}
