﻿using System.Collections;
using UnityEngine;


public class ForgeCharacterDisplayer : MonoBehaviour
{
	public ForgeCharacter forgeCharacter;
	public Animator animator;
	public SpriteRenderer spriteRenderer;


	public void StartEntry()
	{
		animator.SetTrigger("Entry");
		if (forgeCharacter != null)
		{
			spriteRenderer.sprite = forgeCharacter.sprite;
		}
		forgeCharacter = new ForgeCharacter();
		forgeCharacter.weaponQuality = 0;
	}

	public void StartExit()
	{
		animator.SetTrigger("Exit");
		forgeCharacter = null;
	}
}
