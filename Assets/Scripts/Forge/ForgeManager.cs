using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;
public class ForgeManager : MonoBehaviour
{
	public ForgeCharacterDisplayer forgeCharacterDisplayer;
    public bool isForging { get { return _currentCharacter != null; } }

    private ForgeCharacter _currentCharacter;
	public ForgeCharacter currentCharacter { get { return _currentCharacter; } }

	public Sprite[] characterSprites;

	public Vector2 entryMinMaxDelay;
	private float currentDelay;

	public delegate void CharacterSwitch();
	public static event CharacterSwitch OnCharacterSwitch;
	public static event CharacterSwitch OnCharacterleave;


    private void Update()
	{

		if (isForging)
		{
			if (Input.GetKeyDown(KeyCode.Space))
			{
				MakeCharacterLeave();
			}
		}
		else
		{
			currentDelay -= Time.deltaTime;
			if (currentDelay <= 0)
			{
				_currentCharacter = new ForgeCharacter();
				_currentCharacter.sprite = characterSprites[Random.Range(0, characterSprites.Length - 1)];
				forgeCharacterDisplayer.forgeCharacter = _currentCharacter;
				forgeCharacterDisplayer.StartEntry();
				OnCharacterSwitch?.Invoke();
			}
			
		}
			
	}

	public void MakeCharacterLeave()
	{
		OnCharacterleave?.Invoke();
		_currentCharacter = null;
		forgeCharacterDisplayer.StartExit();
		currentDelay = Random.Range(entryMinMaxDelay.x, entryMinMaxDelay.y);
	}	
}
