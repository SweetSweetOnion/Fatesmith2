﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class UIManager : MonoBehaviour
{
	public Button repairButton;
	public Button defenseButton;
	public Button attackButton;
	public Button validateButton;
	public Text soldierText;
	public Text battleText;

	private CharacterComponent current;

	public int metal = 10;

	private void OnEnable()
	{
		Game.Instance.OnCharacterEnter += OnCharacterEnter;
		Game.Instance.OnEndTurn += OnEndTurn;
		repairButton.onClick.AddListener(OnRepair);
		defenseButton.onClick.AddListener(OnDefense);
		attackButton.onClick.AddListener(OnAttack);
		validateButton.onClick.AddListener(OnUpgrade);
		
	}

	

	private void OnDisable()
	{
		Game.Instance.OnCharacterEnter -= OnCharacterEnter;
		repairButton.onClick.RemoveListener(OnRepair);
		defenseButton.onClick.RemoveListener(OnDefense);
		attackButton.onClick.RemoveListener(OnAttack);
		validateButton.onClick.RemoveListener(OnUpgrade);
	}


	private void OnCharacterEnter(CharacterComponent c)
	{
		current = c;
		UpdateSoldierStatus(c);
	}


	private void OnRepair(){
		current.Upgrade(UpgradeType.Repair);
		UpdateSoldierStatus(current);
		metal -= 1;
		UpdateBattleStatus();
		UpdateButton();

	}

	private void OnDefense()
	{
		current.Upgrade(UpgradeType.Defence);
		UpdateSoldierStatus(current);
		metal -= 3;
		UpdateBattleStatus();
		UpdateButton();

	}

	private void OnAttack()
	{

		current.Upgrade(UpgradeType.Attack);
		UpdateSoldierStatus(current);
		metal -= 2;
		UpdateBattleStatus();
		UpdateButton();

	}

	private void OnUpgrade()
	{
		Game.Instance.EndTurn();
		UpdateSoldierStatus(current);
		UpdateBattleStatus();
		UpdateButton();

	}

	private void OnEndTurn()
	{
		metal += Random.Range(2, 4);
		UpdateBattleStatus();
		UpdateButton();

	}

	private void UpdateButton(){
		repairButton.interactable = metal >= 1;
		attackButton.interactable = metal >= 2;
	
		defenseButton.interactable = metal >= 3;
		
	}

	private void UpdateSoldierStatus(CharacterComponent c)
	{
		if (c == null)
		{
			soldierText.text = "";
			return;
		}
		string str = "" + c.name;
		str += "\nLevel : " + c.data.level;
		str += "\nHP : " + c.data.currentHP + "/" + c.data.maxHp;
		str += "\nAttackValue : " + c.data.attackValue;
		str += "\nAttackPriority : " + c.data.attackPriority;
		str += "\nAggroValue : " + c.data.aggroWeight;
		str += "\nDamage dealt : " + c.data.totalDmgDeal;
		str += "\nDamage received : " + c.data.totalDmgReceived;
		str += "\nKill count : " + c.data.killCount;

		soldierText.text = str;
		
	}

	private void UpdateBattleStatus(){
		string str = "FATESMITH 2 ";
		str += "\n current turn : " + Game.Instance.currentTurn;
		str += "\n soldier remaining : " + Game.Instance.ArmyManager.playerArmy.CountAliveSoldiers();
		str += "\n enemy remaining : " + Game.Instance.ArmyManager.enemyArmy.CountAliveSoldiers();
		str += "\n METAL : " + metal;

		battleText.text = str;
	}
	
}
