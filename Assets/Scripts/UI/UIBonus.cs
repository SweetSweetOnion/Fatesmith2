﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class UIBonus : MonoBehaviour
{

	public string text;
	public float duration;
	public Color color;
	public Vector3 speed;

	public TextMeshProUGUI textMesh;


	private void Update()
	{
		textMesh.text = text;
		color.a = Mathf.Min(duration*2, 1);
		textMesh.color = color;

		duration -= Time.deltaTime;
		transform.position += speed * Time.deltaTime;

		if(duration <= 0)
		{
			Destroy(this.gameObject);
		}
	}
}
