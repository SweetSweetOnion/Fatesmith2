using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.EventSystems;

public class SkillButton : MonoBehaviour , IPointerEnterHandler, IPointerExitHandler, IPointerDownHandler, IPointerUpHandler
{
    public Transform container;
    public Image outline;
    public Image timerGauge;
	public Image hoverBackground;
	public Image skillSprite;
    public TextMeshProUGUI timerTextMesh;
	public TextMeshProUGUI inputTextMesh;

	public bool isAvailable;


	public KeyCode skillInput;

	public ForgeSkill forgeSkill;

	public delegate void SkillActivated(ForgeSkill skill);
	public event SkillActivated OnSkillActivated;

	
	
	
	private float currentCooldown;
	private bool isHovered;
	private bool isPress;

	private void OnEnable()
	{
		SetForgeSkill(forgeSkill);
	}

	public void OnPointerDown(PointerEventData eventData)
	{
		isPress = true;
	}

	public void OnPointerUp(PointerEventData eventData)
	{
		isPress = false;
		if (IsClickable() && isHovered)
		{
			ApplyButton();
		}
	}

	public void OnPointerEnter(PointerEventData eventData)
	{
		isHovered = true;
	}

	public void OnPointerExit(PointerEventData eventData)
	{
		isHovered = false;
	}



	private bool IsClickable()
	{
		return currentCooldown <= 0 && isAvailable && forgeSkill != null;
	}

	private void ApplyButton()
	{
		currentCooldown = forgeSkill.cooldown;
		OnSkillActivated?.Invoke(forgeSkill);
	}

	private void Start()
	{
		currentCooldown = forgeSkill.cooldown;
	}

	private void SetForgeSkill(ForgeSkill f)
	{
		if (f == null)
		{
			skillSprite.sprite = null;
			skillSprite.color = new Color(0, 0, 0, 0);
			return;
		}
		forgeSkill = f;
		skillSprite.sprite = f.skillSprite;
		skillSprite.color = new Color(1,1,1,1);
		currentCooldown = f.cooldown;

	}

	private void Update()
	{
		if (forgeSkill == null) return;
		if (!isAvailable)
		{
			timerGauge.enabled = true;
			timerGauge.fillAmount = 1;
			timerTextMesh.enabled = false;
			return;
		}

		if (currentCooldown > 0)
		{
			currentCooldown -= Time.deltaTime;
			timerTextMesh.text = currentCooldown.ToString("0.00");
			timerGauge.fillAmount = currentCooldown / forgeSkill.cooldown;
			timerGauge.enabled = true;
			timerTextMesh.enabled = true;
		}
		else
		{
			timerGauge.enabled = false;
			timerTextMesh.enabled = false;
		}

		if (Input.GetKeyDown(skillInput))
		{
			isPress = true;
		}

		if (Input.GetKeyUp(skillInput))
		{
			isPress = false;
			if (IsClickable())
			{
				ApplyButton();
			}
		}

		hoverBackground.enabled = isHovered && IsClickable();


		inputTextMesh.text = skillInput.ToString();

		outline.enabled = IsClickable();
		

		if (isPress && IsClickable())
		{
			timerGauge.enabled = true;
			timerGauge.fillAmount = 1;
			container.localScale = Vector3.one*0.9f;
			outline.enabled = false;
			hoverBackground.enabled = false;
		}
		else
		{
			container.localScale = Vector3.one * 1f;
		}
	}
}
