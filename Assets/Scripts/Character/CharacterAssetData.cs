﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;

[CreateAssetMenu(fileName = "CharacterAssetData", menuName = "Data/CharacterAssetData")]
[InlineEditor]
public class CharacterAssetData : ScriptableObject
{
	//health
	[BoxGroup("Health")]
	[MinMaxSlider(0,1000,true)]
	public Vector2 maxHp;


	//attack
	[BoxGroup("Attack")]
	[MinMaxSlider(0, 1000, true)]
	public Vector2 attackValue;
	

	[BoxGroup("Attack")]
	[MinMaxSlider(0, 1000, true)]
	public Vector2 attackPriority;//not that usefull ?


	[BoxGroup("Defense")]
	[MinMaxSlider(0, 1000, true)]
	public Vector2 aggroWeight;

}
