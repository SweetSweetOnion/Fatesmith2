﻿using UnityEngine;
using System.Collections;
using Sirenix.OdinInspector;

public class CharacterComponent : MonoBehaviour
{
	
	[ShowInInspector]
	private CharacterAssetData assetData;

	[SerializeField]
	public CharacterData data;



	public bool isDead = false;
	private ArmyComponent _army;
	public ArmyComponent army { get { return _army; } }

	public void Init(ArmyComponent a)
	{
		_army = a;
		InitData();
	}

	[Button("Init Data")]
	public void InitData(){
		data.name = GenerateName(Random.Range(3, 8)) + " " + GenerateName(Random.Range(0, 2)) + " " + GenerateName(Random.Range(5, 8));
		data.Init(assetData);
	}
	private void OnEnable()
	{
		
		float rHP = (int)Random.Range(data.maxHp/2, data.maxHp*2);
		data.currentHP = rHP;
		data.maxHp = rHP;
		data.attackPriority = (int)Random.Range(data.attackPriority / 2, data.attackPriority * 2);
		data.attackValue = (int)Random.Range(data.attackValue / 2, data.attackValue * 2);
		data.aggroWeight = (int)Random.Range(data.aggroWeight / 2, data.aggroWeight * 2);

	}

	public void Upgrade(UpgradeType type){

		gameObject.name = "| " + gameObject.name;
		data.level++;
		
		switch (type)
		{
			case UpgradeType.Repair:
				data.level--;
				data.currentHP += data.maxHp / 2;
				data.currentHP = Mathf.Min(data.currentHP, data.maxHp);
				break;
			case UpgradeType.Attack:
				data.attackValue += Random.Range(5, 10);
				data.attackPriority += Random.Range(10, 20);
				break;
			case UpgradeType.Defence:
				int addHp = Random.Range(20, 40);
				data.currentHP += addHp;
				data.maxHp += addHp;
				data.aggroWeight = Mathf.Max(1, data.aggroWeight + Random.Range(-20, -10));
				break;
			default:
				break;
		}
	}

	public bool ReceiveDamage(float dmg){
		data.currentHP -= dmg; 
		data.totalDmgReceived += dmg;
		if (data.currentHP < 0){
			data.currentHP = 0;
			Dead();
			return true;
		}
		return false;
		
	}

	public void Dead(){
		gameObject.name = "DEAD " + gameObject.name;
		isDead = true;
		_army.UpdateAliveSoldier();
	}

	public void Fight(CharacterComponent c){
		bool kill = c.ReceiveDamage(data.attackValue);
		data.totalDmgDeal += data.attackValue;
		if (kill) data.killCount++;
	}

	public string GenerateName(int length){
		var chars = "abcdefghijklmnopqrstuvwxyz";
		string str = "";

		for (int i = 0; i < length; i++)
		{
			str += chars[Random.Range(0,chars.Length)];
		}

		return UppercaseFirst(str);
	}

	private string UppercaseFirst(string s)
	{
		if (string.IsNullOrEmpty(s))
		{
			return string.Empty;
		}
		char[] a = s.ToCharArray();
		a[0] = char.ToUpper(a[0]);
		return new string(a);
	}
}

public enum UpgradeType{
	Repair,Attack,Defence
}
