using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;

[System.Serializable]
public class CharacterData
{
	//experience
	[ReadOnly]
	public string name = "";
	[ReadOnly]
	public float level = 0;
	//health
	[BoxGroup("Health"),ReadOnly]
	public float maxHp = 0;
	[BoxGroup("Health"), ReadOnly]
	public float currentHP = 0;

	//attack
	[BoxGroup("Attack"), ReadOnly]
	public float attackValue = 0;
	[BoxGroup("Attack"), ReadOnly]
	public float attackPriority = 0;//not that usefull ?

	//defence
	[BoxGroup("Defense"), ReadOnly]
	public float aggroWeight = 0;


	//info
	[BoxGroup("Info"), ReadOnly]
	public float totalDmgDeal = 0;
	[BoxGroup("Info"), ReadOnly]
	public float totalDmgReceived = 0;
	[BoxGroup("Info"), ReadOnly]
	public int killCount = 0;


	public void Init(CharacterAssetData chrAssetData){
		if (!chrAssetData) return;
		maxHp = (int)minMaxRandom(chrAssetData.maxHp);
		currentHP = maxHp;
		attackValue = (int)minMaxRandom(chrAssetData.attackValue);
		attackPriority = (int)minMaxRandom(chrAssetData.attackPriority);
		aggroWeight = (int)minMaxRandom(chrAssetData.aggroWeight);
	}

	private float minMaxRandom(Vector2 minmax){
		return Random.Range(minmax.x, minmax.y);
	}
}
