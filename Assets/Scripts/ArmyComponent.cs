﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ArmyComponent : MonoBehaviour
{

	public ArmyTeam armyTeam;
	public int soldierAmount;
	public CharacterComponent soldierPrefab;
	public bool IsDead{ get { return _aliveSoldiers.Count == 0; } }


	private List<CharacterComponent> _soldiers = new List<CharacterComponent>();
	private List<CharacterComponent> _aliveSoldiers = new List<CharacterComponent>();
	private int _currentSoldierID = 0;

	public void Init(){
		for(int i= 0; i< soldierAmount; i++){
			var inst = Instantiate(soldierPrefab, transform);
			_soldiers.Add(inst);
			_aliveSoldiers.Add(inst);
			inst.Init(this);
		}
	}

	public void Reinforce(int amount){
		for (int i = 0; i < amount; i++)
		{
			var inst = Instantiate(soldierPrefab, transform);
			_soldiers.Add(inst);
			_aliveSoldiers.Add(inst);
			inst.Init(this);
		}
	}

	public void UpdateAliveSoldier(){
		_aliveSoldiers.Clear();
		foreach (var s in _soldiers)
		{
			if (!s.isDead)
			{
				_aliveSoldiers.Add(s);
			}
		}

	}

	public CharacterComponent GetRandomSoldier(bool aliveOnly = false){
		if (aliveOnly)
		{
			if (_aliveSoldiers.Count == 0) return null;
			return _aliveSoldiers[Random.Range(0, _aliveSoldiers.Count)];
		}
		else{
			if (_soldiers.Count == 0) return null;
			return _soldiers[Random.Range(0, _soldiers.Count)];
		}
	}

	private float GetTotalAliveAggroWeight(){
		float c = 0;
		foreach(var s in _aliveSoldiers){
			c += s.data.aggroWeight;
		}
		return c;
	}

	public CharacterComponent GetRandomWeightedAliveSoldier()
	{
		float r = Random.Range(0, GetTotalAliveAggroWeight());
		float c = 0;
		foreach (var s in _aliveSoldiers)
		{
			c += s.data.aggroWeight;
			if(r <= c){
				return s;
			}
		}
		return null;
	}

	public CharacterComponent GetCurrentSoldier(){
		return _soldiers[_currentSoldierID];
	}

	public CharacterComponent GetNextAliveSoldier(){
		if (_aliveSoldiers.Count == 0) return null;

		for(int i = 0; i < _soldiers.Count; i++){
			_currentSoldierID = (_currentSoldierID + 1) % _soldiers.Count;
			if(!_soldiers[_currentSoldierID].isDead){
				return _soldiers[_currentSoldierID];
			}
		}
		return null;
	}

	public int CountSoldiers() { return _soldiers.Count; }
	public int CountAliveSoldiers(){ return _aliveSoldiers.Count; }

	public List<CharacterComponent> GetAliveSoldiers(){ return _aliveSoldiers; }
}

public enum ArmyTeam{
	Player, Enemy
}
