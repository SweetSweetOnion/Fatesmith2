﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ArmyManager : MonoBehaviour
{
	private Game game;
	[HideInInspector]
	public ArmyComponent playerArmy;
	[HideInInspector]
	public ArmyComponent enemyArmy;

	public ArmyComponent playerArmyPrefab;
	public ArmyComponent enemyArmyPrefab;

	private void Awake()
	{
		game = GetComponentInParent<Game>();
	}
	public void InitArmy(){
		playerArmy = Instantiate(playerArmyPrefab);
		playerArmy.Init();
		enemyArmy = Instantiate(enemyArmyPrefab);
		enemyArmy.Init();
	}
}
