﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class BattleManager : MonoBehaviour
{

	private Game game;

	public void Awake()
	{
		game = GetComponentInParent<Game>();
	}

	public void SolveFight(){
		if (game.ArmyManager.playerArmy.IsDead || game.ArmyManager.enemyArmy.IsDead) return;

		List<CharacterComponent> aliveChars = new List<CharacterComponent>();
		aliveChars.AddRange(game.ArmyManager.playerArmy.GetAliveSoldiers());
		aliveChars.AddRange(game.ArmyManager.enemyArmy.GetAliveSoldiers());
		aliveChars.Sort((P1, p2) => P1.data.attackPriority.CompareTo(p2.data.attackPriority));

		for(int i = 0; i< aliveChars.Count; i++)
		{
			
			if(aliveChars[i].isDead){
				continue;
			}
			if(aliveChars[i].army.armyTeam == ArmyTeam.Enemy){
				var target = game.ArmyManager.playerArmy.GetRandomWeightedAliveSoldier();
				if(target)
				aliveChars[i].Fight(target);
			}else{
				var target = game.ArmyManager.enemyArmy.GetRandomWeightedAliveSoldier();
				if (target)
					aliveChars[i].Fight(target);
			}
		}
	}
}
