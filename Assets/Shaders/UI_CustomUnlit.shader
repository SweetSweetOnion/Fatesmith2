Shader "Colorize"
{
    Properties
    {
        _MainTex("_MainTex", 2D) = "white" {}      // Note _MainTex is a special name: This can also be accessed from C# via mainTexture property. 
        Colorize("Colorize", Range(0.0, 1.0)) = 1
    }
        SubShader
        {
            Pass
            {
            Name "ColorizeSubshader"

            // ---
            // For Alpha transparency:   https://docs.unity3d.com/462/Documentation/Manual/SL-SubshaderTags.html
            Tags
            {
                "Queue" = "Transparent"
                "RenderType" = "Transparent"
            }
            Blend SrcAlpha OneMinusSrcAlpha
            // ---

            CGPROGRAM
            #pragma vertex   MyVertexShaderFunction 
            #pragma fragment  MyFragmentShaderFunction
            #pragma fragmentoption ARB_precision_hint_fastest 
            #include "UnityCG.cginc"

            sampler2D _MainTex;

            float Colorize;
           
            struct my_needed_data_from_unity
            {
                float4 vertex   : POSITION;  // The vertex position in model space.          //  Name&type must be the same!
                float4 texcoord : TEXCOORD0; // The first UV coordinate.                     //  Name&type must be the same!
                float4 color    : COLOR;     //    The color value of this vertex specifically. //  Name&type must be the same!
            };

            // my custom Vertex to Fragment struct
            struct my_v2f
            {
                float4  pos : SV_POSITION;
                float2  uv : TEXCOORD0;
                float4  color : COLOR;
            };

            my_v2f  MyVertexShaderFunction(my_needed_data_from_unity  v)
            {
                my_v2f  result;
                result.pos = UnityObjectToClipPos(v.vertex);  // Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'
                result.uv = v.texcoord.xy;
                result.color = v.color;
                return result;
            }

            float4 MyFragmentShaderFunction(my_v2f  i) : COLOR
            {
                float4 texcolor = tex2D(_MainTex, i.uv); // texture's pixel color
                float4 vertexcolor = i.color; // this is coming from UnityEngine.UI.Image.Color
                texcolor.rgb = texcolor.rgb * (1 - Colorize) + vertexcolor.rgb * Colorize;
                texcolor.a = vertexcolor.a * texcolor.a;
                return texcolor;
            }

            ENDCG
        }
        }
            //Fallback "Diffuse"
}